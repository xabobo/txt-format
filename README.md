# TxtFormat 

#### 介绍

文本文件编码格式转换工具，支持csv，txt等文本文件编码格式转换。
主要用作cocoscreator等游戏的csv配置文件的编码格式自动转换。

开发初衷，WPS编辑保存后的csv文件编码格式不是常用的UTF-8,虽然可以用记事本打开另存为，但是文件一旦多了很麻烦。
有了这个自动工具，只要仍在csv文件所在目录下，点击【全部转换】，就会自动将目录下的所有csv文件进行转换，并替换原有文件。

#### 软件架构
VS2013 WinForm

#### 使用教程
1.把TxtFormat.exe（txt-format/TxtFormat/bin/Release/TxtFormat.exe）放在*.csv文件所在目录下面。
2.选择要转换到的编码格式。
3.点击【全部转换】或者【选中转换】。

#### 软件截图
![输入图片说明](https://images.gitee.com/uploads/images/2020/1227/120857_db393471_587340.png "屏幕截图.png")