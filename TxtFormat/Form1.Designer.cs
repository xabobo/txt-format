﻿namespace TxtFormat
{
    partial class TxtFormat
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.AllBtn = new System.Windows.Forms.Button();
            this.selectBtn = new System.Windows.Forms.Button();
            this.encodeCb = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.fileNameTB = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // AllBtn
            // 
            this.AllBtn.Location = new System.Drawing.Point(52, 87);
            this.AllBtn.Name = "AllBtn";
            this.AllBtn.Size = new System.Drawing.Size(75, 65);
            this.AllBtn.TabIndex = 0;
            this.AllBtn.Text = "全部转换";
            this.AllBtn.UseVisualStyleBackColor = true;
            this.AllBtn.Click += new System.EventHandler(this.AllBtn_Click);
            // 
            // selectBtn
            // 
            this.selectBtn.Location = new System.Drawing.Point(191, 87);
            this.selectBtn.Name = "selectBtn";
            this.selectBtn.Size = new System.Drawing.Size(75, 65);
            this.selectBtn.TabIndex = 0;
            this.selectBtn.Text = "选中转换";
            this.selectBtn.UseVisualStyleBackColor = true;
            this.selectBtn.Click += new System.EventHandler(this.selectBtn_Click);
            // 
            // encodeCb
            // 
            this.encodeCb.FormattingEnabled = true;
            this.encodeCb.Items.AddRange(new object[] {
            "UTF-8",
            "ANSI",
            "Unicode"});
            this.encodeCb.Location = new System.Drawing.Point(145, 22);
            this.encodeCb.Name = "encodeCb";
            this.encodeCb.Size = new System.Drawing.Size(121, 20);
            this.encodeCb.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(50, 25);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(89, 12);
            this.label1.TabIndex = 2;
            this.label1.Text = "目标编码格式：";
            // 
            // label2
            // 
            this.label2.ForeColor = System.Drawing.Color.Red;
            this.label2.Location = new System.Drawing.Point(31, 178);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(286, 71);
            this.label2.TabIndex = 2;
            this.label2.Text = "1.【全部转换】会将目录下的所有csv文件转换。\r\n\r\n2.若要转换其他文件，请使用【选中转换】。\r\n\r\n3.转换会自动覆盖原文件，可以根据需要自行备份。";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(50, 57);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(65, 12);
            this.label3.TabIndex = 2;
            this.label3.Text = "目标文件：";
            // 
            // fileNameTB
            // 
            this.fileNameTB.Location = new System.Drawing.Point(145, 57);
            this.fileNameTB.Name = "fileNameTB";
            this.fileNameTB.ReadOnly = true;
            this.fileNameTB.Size = new System.Drawing.Size(121, 21);
            this.fileNameTB.TabIndex = 3;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(260, 249);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(65, 12);
            this.label4.TabIndex = 4;
            this.label4.Text = "作者：BOBO";
            // 
            // TxtFormat
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(329, 267);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.fileNameTB);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.encodeCb);
            this.Controls.Add(this.selectBtn);
            this.Controls.Add(this.AllBtn);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.Name = "TxtFormat";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "文本文件编码格式转换工具";
            this.Load += new System.EventHandler(this.TxtFormat_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button AllBtn;
        private System.Windows.Forms.Button selectBtn;
        private System.Windows.Forms.ComboBox encodeCb;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox fileNameTB;
        private System.Windows.Forms.Label label4;
    }
}

